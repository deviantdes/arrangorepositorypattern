﻿using ArangoDB.Client;

namespace ArrangoRepositoryPattern.DBModels
{
    public class TestArrango
    {
        [DocumentProperty(Identifier = IdentifierType.Key)]
        public string Key{ get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public int Age { get; set; }
    }
}
