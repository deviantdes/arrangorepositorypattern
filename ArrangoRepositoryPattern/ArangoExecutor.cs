﻿using ArangoDB.Client;
using System;

namespace ArrangoRepositoryPattern
{
    public class ArangoExecutor
    {
        DatabaseSharedSetting _settings;

        public ArangoExecutor()
        {
            _settings = new DatabaseSharedSetting
            {
                Url = "http://localhost:8529",
                Database = "TestArrangoDB",
                Credential = new System.Net.NetworkCredential("root", "123456")
            };
        }

        public void Execute(Action<ArangoDatabase> action)
        {
            using (var db = new ArangoDatabase(_settings))
            {
                action(db);
            }
        }
    }
}
