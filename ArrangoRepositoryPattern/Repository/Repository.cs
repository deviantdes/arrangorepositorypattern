﻿using ArangoDB.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ArrangoRepositoryPattern.Repository
{
    public class Repository<T> : IRepository<T> where T : class
    {
        private ArangoExecutor _arangoExecutor;

        public Repository()
        {
            _arangoExecutor = new ArangoExecutor();
        }
        public void Delete(T entity)
        {
            throw new NotImplementedException();
        }

        public IQueryable<T> GetAll()
        {
            throw new NotImplementedException();
        }

        public T GetById(int id)
        {
            throw new NotImplementedException();
        }

        public void Insert(T entity)
        {
            _arangoExecutor.Execute(db => {
                db.Insert<T>(entity);
            });
        }

        public IQueryable<T> SearchFor(Expression<Func<T, bool>> predicate)
        {
            throw new NotImplementedException();
        }
    }
}
