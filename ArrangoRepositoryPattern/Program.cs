﻿using ArangoDB.Client;
using ArrangoRepositoryPattern.DBModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArrangoRepositoryPattern
{
    class Program
    {
        static void Main(string[] args)
        {

            var settings = new DatabaseSharedSetting
            {
                Url = "http://localhost:8529",
                Database = "TestArrangoDB",
                Credential = new System.Net.NetworkCredential("root", "123456")
            };


            using (var db = new ArangoDatabase(settings))
            {
                ///////////////////// insert and update documents /////////////////////////

                var test = new TestArrango { Name = "raoof hojat", Age = 26, Address="asd" };

                // insert new document and creates 'Person' collection on the fly
                db.Insert<TestArrango>(test);
            }
        }
    }
}
